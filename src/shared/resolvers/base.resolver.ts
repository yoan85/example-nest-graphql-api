// import {Args, Resolver, Query} from '@nestjs/graphql';
import {ClassType, Resolver} from 'type-graphql';

/**
 * Example of property and Query
 * protected items: T[] = [];
 * @Query(type => [objectTypeCls], { name: `getAll${suffix}` })
 * async getAll(@Arg("first", type => Int) first: number): Promise<T[]> {
 *  return this.items.slice(0, first);
 * }
 *
 */
export function createBaseResolver<T extends ClassType>(suffix: string, objectTypeCls: T) {
  @Resolver({isAbstract: true})
  abstract class BaseResolver {
    // code here
  }

  return BaseResolver;
}
