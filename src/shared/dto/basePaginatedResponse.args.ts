import {ClassType, Field, ObjectType, Int} from 'type-graphql';

export function BasePaginatedResponse<TItem>(TItemClass: ClassType<TItem>) {
  // `isAbstract` decorator option is mandatory to prevent registering in schema
  @ObjectType({isAbstract: true})
  abstract class PaginatedResponseClass {
    // here we use the runtime argument
    @Field(type => [TItemClass])
    // and here the generic type
    items: TItem[];

    @Field(type => Int)
    // total of elements
    total: number;

    @Field()
    // count element returned
    count: number;
  }
  return PaginatedResponseClass;
}
