import {MigrationInterface, QueryRunner} from 'typeorm';

export class CreateFoobarTable1574005337839 implements MigrationInterface {
  name = 'CreateFoobarTable1574005337839';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "foobar" ("id" SERIAL NOT NULL, "isActive" boolean NOT NULL DEFAULT true, "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, CONSTRAINT "PK_6989de59bde3045c410160974af" PRIMARY KEY ("id"))`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "foobar"`, undefined);
  }
}
