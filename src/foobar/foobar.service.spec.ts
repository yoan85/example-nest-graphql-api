import {Test, TestingModule} from '@nestjs/testing';
import {FoobarService} from './foobar.service';
import {FoobarRepository} from './repositories/foobar.respository';

describe('FoobarService', () => {
  let service: FoobarService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoobarService, FoobarRepository],
    }).compile();

    service = module.get<FoobarService>(FoobarService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
