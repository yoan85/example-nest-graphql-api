import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {FoobarService} from './foobar.service';
import {FoobarResolver} from './foobar.resolver';
import {FoobarRepository} from './repositories/foobar.respository';

@Module({
  imports: [TypeOrmModule.forFeature([FoobarRepository])],
  providers: [FoobarService, FoobarResolver],
})
export class FoobarModule {}
