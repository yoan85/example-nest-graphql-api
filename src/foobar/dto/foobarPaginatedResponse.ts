import {ObjectType} from 'type-graphql';
import {BasePaginatedResponse} from '../../shared/dto/basePaginatedResponse.args';
import {Foobar} from '../entities/foobar.entity';

@ObjectType({description: 'Foobar List Output'})
export class FoobarPaginatedResponse extends BasePaginatedResponse(Foobar) {
  // Redefine here
}
