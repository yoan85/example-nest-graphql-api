import {Field, InputType} from 'type-graphql';
import {Foobar} from '../entities/foobar.entity';

@InputType({description: 'Foobar data to add'})
export class FoobarInputType implements Partial<Foobar> {
  // code here
  @Field()
  firstName: string;

  @Field()
  lastName: string;
}
