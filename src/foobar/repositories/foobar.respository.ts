import {EntityRepository, Repository} from 'typeorm';
import {Foobar} from '../entities/foobar.entity';
import {BaseRepository} from '../../shared/repositories/base.repository';
import {FoobarPaginationArgs} from '../dto/foobarPagination.args';

@EntityRepository(Foobar)
export class FoobarRepository extends BaseRepository<Foobar> {
  getAllFoobar = async () => {
    try {
      return await this.getAll();
    } catch (e) {
      throw new Error(`Error in Repository to find all foobars: ${e}`);
    }
  };

  getAllPaginateFoobar = async (foobarArgs: FoobarPaginationArgs) => {
    try {
      const filter: any = {take: foobarArgs.pageSize, skip: foobarArgs.pageNumber - 1, order: {id: 'ASC'}};
      return await this.findAndCount(filter);
    } catch (e) {
      throw new Error(`Error in Repository to find all foobars, with paginate: ${e}`);
    }
  };

  addFoobar = async (foobarInput: Foobar) => {
    try {
      return await this.save(foobarInput);
    } catch (e) {
      throw new Error(`Error in Repository to add foobars: ${e}`);
    }
  };
}
